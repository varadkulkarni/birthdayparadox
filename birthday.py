#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      Varad
#
# Created:     29-05-2013
# Copyright:   (c) Varad 2013
# Licence:     <your licence>
#-------------------------------------------------------------------------------

import wx
import wx.richtext
import random
import math
import sys
from functools import partial
import matplotlib
matplotlib.use('WXAgg')
from matplotlib.backends.backend_wxagg import FigureCanvasWxAgg as FigureCanvas
from matplotlib.backends.backend_wx import NavigationToolbar2Wx
from matplotlib.figure import Figure

class mainFrame ( wx.Frame ):


        def __init__( self, parent ):
                wx.Frame.__init__ ( self, parent, id = wx.ID_ANY, title = u"Birthday Paradox", pos = wx.DefaultPosition, size = wx.Size( 1192,532 ), style = wx.DEFAULT_FRAME_STYLE|wx.TAB_TRAVERSAL )

                self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )

                fgSizer2 = wx.FlexGridSizer( 1, 2, 0, 0 )
                fgSizer2.AddGrowableCol( 1 )
                fgSizer2.SetFlexibleDirection( wx.BOTH )
                fgSizer2.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )

                bSizer3 = wx.BoxSizer( wx.VERTICAL )

                self.m_staticline3 = wx.StaticLine( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LI_HORIZONTAL )
                bSizer3.Add( self.m_staticline3, 0, wx.EXPAND |wx.ALL, 5 )

                self.m_staticText4 = wx.StaticText( self, wx.ID_ANY, u"The Birthday paradox finds the probability of any two people having the same birthday in a group of people. Here, each group will be tested for any two people having the same birthday.  A number of random samples will be taken for a group and their average will be taken.", wx.DefaultPosition, wx.DefaultSize, 0 )
                self.m_staticText4.Wrap( 500 )
                bSizer3.Add( self.m_staticText4, 0, wx.ALL, 5 )

                self.m_staticline2 = wx.StaticLine( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LI_HORIZONTAL )
                bSizer3.Add( self.m_staticline2, 0, wx.EXPAND |wx.ALL, 5 )

                fgSizer1 = wx.FlexGridSizer( 1, 5, 0, 0 )
                fgSizer1.AddGrowableCol( 0 )
                fgSizer1.SetFlexibleDirection( wx.BOTH )
                fgSizer1.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_ALL )

                self.m_staticText1 = wx.StaticText( self, wx.ID_ANY, u"Number of People", wx.DefaultPosition, wx.DefaultSize, 0 )
                self.m_staticText1.Wrap( -1 )
                fgSizer1.Add( self.m_staticText1, 0, wx.ALL, 5 )

                self.m_textPeople = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
                fgSizer1.Add( self.m_textPeople, 0, wx.ALL, 5 )

                self.m_staticText2 = wx.StaticText( self, wx.ID_ANY, u"Number of Samples", wx.DefaultPosition, wx.DefaultSize, 0 )
                self.m_staticText2.Wrap( -1 )
                fgSizer1.Add( self.m_staticText2, 0, wx.ALL, 5 )

                self.m_textSamples = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
                fgSizer1.Add( self.m_textSamples, 0, wx.ALL, 5 )

                self.m_button3 = wx.Button( self, wx.ID_ANY, u"Calculate", wx.DefaultPosition, wx.DefaultSize, 0 )
                fgSizer1.Add( self.m_button3, 0, wx.ALL, 5 )

                bSizer3.Add( fgSizer1, 0, wx.ALL, 5 )

                self.m_staticline4 = wx.StaticLine( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LI_HORIZONTAL )
                bSizer3.Add( self.m_staticline4, 0, wx.EXPAND |wx.ALL, 5 )

                self.m_staticText5 = wx.StaticText( self, wx.ID_ANY, u"The graph of the number of people in a group vs the average matches is shown. \nThe theoretical probability accrording to the Birthday Paradox will be shown as a blue line and the actual sample points will be plotted in red.\n e.g. For 100 people with 100 samples per group. Average match percentage for all groups having 1 to 100 people will be plotted.\n According to the birthday paradox, for 23 people the match percentage reaches 50% and for 57 people, it reaches 100%", wx.DefaultPosition, wx.DefaultSize, 0 )
                self.m_staticText5.Wrap( 500 )
                bSizer3.Add( self.m_staticText5, 0, wx.ALL, 5 )

                self.m_richText1 = wx.richtext.RichTextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_READONLY|wx.VSCROLL|wx.HSCROLL|wx.NO_BORDER|wx.WANTS_CHARS )
                self.m_richText1.SetMaxSize( wx.Size( -1,200 ) )

                bSizer3.Add( self.m_richText1, 1, wx.EXPAND |wx.ALL, 5 )

                fgSizer2.Add( bSizer3, 1, wx.EXPAND, 5 )

                self.m_panel1 = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.Size( -1,500 ), wx.TAB_TRAVERSAL )
                self.m_panel1.SetMinSize( wx.Size( -1,500 ) )

                fgSizer2.Add( self.m_panel1, 1, wx.EXPAND |wx.ALL, 5 )

                self.SetSizer( fgSizer2 )
                self.Layout()

                self.Centre( wx.BOTH )

                self.m_button3.Bind( wx.EVT_BUTTON, partial(on_calculate,frame=self) )


        def __del__( self ):
                pass


def on_calculate(self,frame):

    val1=frame.m_textPeople.GetValue()

    if (len(val1)<1):
        return

    maxpeople=int(frame.m_textPeople.GetValue())
    numsamples=int(frame.m_textSamples.GetValue())

    frame.m_richText1.SetValue("")

    lim=max(maxpeople,numsamples)

    xaxis=[]
    yaxis= []
    maxis=[]
    matchesarr=[0]*lim


    #ax.xlim([0,lim])
    #pyploy.ylim([0,lim])

    fig=Figure()
    ax = fig.add_subplot(111)
    canvas = FigureCanvas(frame.m_panel1,-1,fig)

    ax.set_ylabel("Match Percentage")
    ax.set_xlabel("Number of People")
    ax.set_xlim((0,maxpeople+10))
    ax.set_ylim((0,110))

    val=0
    for i in range(0,maxpeople):
        match=0
        for j in range(0,numsamples):

            arr = [random.randrange(1, 366) for _ in range(0, i)]

            k=0
            while (k < len(arr) and k>-1):
                l=k+1
                while (l<len(arr)-1):
                    if (arr[k]==arr[l]):
                        k=-1
                        break
                    l = l + 1
                if(k==-1):
                    break
                k = k + 1

            if (k==-1):
                match=match+1
        matchesarr[i]=match
        yaxis.append((match/float(numsamples))*100.0)
        val=i*(i-1)
        val=val/2
        maxis.append((1.0-(math.pow((364.0/365.0),(float(val)))))*100.0)
        xaxis.append(i)
        ax.plot(xaxis,yaxis,"ro",xaxis,maxis,"b")
        canvas.draw()

    for i in xaxis:
        string="\nFor "+ str(i+1) +" people in a group with "+str(numsamples)+" samples,"
        string = string +" the number of matches are " + str(matchesarr[i])
        frame.m_richText1.AppendText(string)



class MyApp(wx.App):

    def OnInit(self):

        frame = mainFrame(None)
        frame.Show(True)

        self.SetTopWindow(frame)

        return True



app = MyApp(0)
app.MainLoop()
